package com.example.user.prestocloud_data_collector.communication_utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.example.user.prestocloud_data_collector.CommunicationClient;

public class TransmitTriggerReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String brokerUri = "tcp://broker.example.com";
        Log.d("BroadcastReceiver", "Recurring alarm; requesting download service.");
        // start the download
        Intent transmitter = new Intent(context, CommunicationClient.class);
        transmitter.setData(Uri
                .parse(brokerUri));
        context.startService(transmitter);
    }
}
