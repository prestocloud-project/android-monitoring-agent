package com.example.user.prestocloud_data_collector;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import static com.example.user.prestocloud_data_collector.SensorDataPresenterActivity.CSV_SEPARATOR;
import static com.example.user.prestocloud_data_collector.SensorDataPresenterActivity.FIELD_SEPARATOR;
import static com.example.user.prestocloud_data_collector.SensorDataPresenterActivity.SPEC_MQTT_TOPIC;
import static com.example.user.prestocloud_data_collector.SensorDataPresenterActivity.sensors_status;
import static com.example.user.prestocloud_data_collector.SensorDataPresenterActivity.sensors_values;

public class DeviceInformationManager extends Service {

    public static File application_directory;
    public static File persistence_file;
    public static File id_file;
    public static String device_id="";
    public static SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yy_HH.mm.ss");

    public static final int LOGGING_INTERVAL = 10; //in seconds
    private static final String ALLOWED_CHARACTERS ="0123456789qwertyuiopasdfghjklzxcvbnm";
    private static int seconds_elapsed = 0;
    public static boolean delete_data_collected_file = false;

    public DeviceInformationManager() {
    }

    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent,int flags ,int startId){

        //check if omitting mContext is OK :application_directory= SensorDataPresenterActivity.mContext.getDir("Data", Context.MODE_PRIVATE);
        application_directory= this.getDir("Data", Context.MODE_PRIVATE);
        initializePersistenceFile();
        id_file = new File(application_directory,"id_file");

        InitializeDevice();

        ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
        scheduler.scheduleWithFixedDelay
                (new Runnable() {
                    public void run() {
                        if (delete_data_collected_file){
                            persistence_file.delete();
                            delete_data_collected_file = false;
                        }
                        int i = 0;
                        try {
                            persistence_file.createNewFile();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        //Write the available data to the file, including at the beginning the communication protocol version.
                        write_data(SensorDataPresenterActivity.COMMUNICATION_PROTOCOL_VERSION+CSV_SEPARATOR);
                        synchronized (sensors_values) {
                            while (i < sensors_values.size()) {
                                write_data(sensors_values.get(i) + CSV_SEPARATOR);
                                i++;
                            }
                        }
                        write_data(sdf.format(new Timestamp(System.currentTimeMillis())));
                        write_data(System.getProperty("line.separator"));

                    }
        }, 0, LOGGING_INTERVAL, TimeUnit.SECONDS);
        //Log.d("PERSISTENCE",getFileData());





        // We want this service to continue running until it is explicitly
        // stopped, so return sticky.
        return START_STICKY;

    }

    public static void initializePersistenceFile() {
        persistence_file =  new File(application_directory, "data_collected"); //Getting a file within the dir.
    }

    private synchronized void write_data(String data) {
        try {
            FileWriter fw;
            BufferedWriter bw;
            if (persistence_file.exists()) {
                fw = new FileWriter(persistence_file, true);
                bw = new BufferedWriter(fw);
            } else{
                fw = new FileWriter(persistence_file);
                bw = new BufferedWriter(fw);
                bw.append(device_id+System.getProperty("line.separator"));
            }
            bw.append(data);
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getFileData(String filename){
        BufferedReader br = null;
        FileReader fr = null;
        String fileContent = "";
        try {

            fr = new FileReader(new File(filename));
            br = new BufferedReader(fr);
            StringBuffer sb = new StringBuffer();
            String sCurrentLine;

            while ((sCurrentLine = br.readLine()) != null) {
                sb.append(sCurrentLine);
            }
            fileContent =  sb.toString();


        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null)
                    br.close();
                if (fr != null)
                    fr.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return  fileContent;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public void InitializeDevice(){
        FileReader fr;
        BufferedReader br;
        try {
            if (!(id_file.exists())) {
                initializeDeviceFile();
                sendDeviceFile();
            }else{
               //read file to get device id
                fr = new FileReader(id_file);
                br = new BufferedReader(fr);
                device_id = br.readLine().trim();
            }
            try {
                sensors_status.set(Sensor_field.device_id, "Device ID: " + device_id);
                synchronized (sensors_values) {
                sensors_values.set(Sensor_field.device_id, device_id); //assumes that initialization of the structure has taken place before any services are invoked.
                }
            }catch (IndexOutOfBoundsException e){
                sensors_status.add(Sensor_field.device_id, "Device ID: " + device_id);
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    private boolean sendDeviceFile() {

        boolean device_file_sent=false,verbose = false;
        int i,cores = getNumberOfCores();
        String message  = "";
        if (verbose) {
            message += "The device CPU is: "+getCPUModel()+CSV_SEPARATOR;
        }else{
            message += getCPUModel()+CSV_SEPARATOR;
        }
        for (i=0;i<cores;i++){
            if (i==cores-1) {
                if (verbose) {
                    message += "Maximum CPU" + i + " frequency: " + getFileData("/sys/devices/system/cpu/cpu" + i + "/cpufreq/cpuinfo_max_freq")+ FIELD_SEPARATOR;
                    message += "Minimum CPU" + i + " frequency: " + getFileData("/sys/devices/system/cpu/cpu" + i + "/cpufreq/cpuinfo_min_freq");
                } else {
                    message += getFileData("/sys/devices/system/cpu/cpu" + i + "/cpufreq/cpuinfo_max_freq")+ FIELD_SEPARATOR;
                    message += getFileData("/sys/devices/system/cpu/cpu" + i + "/cpufreq/cpuinfo_min_freq");
                }
            }
            else{
                if (verbose) {
                    message += "Maximum CPU" + i + " frequency: " + getFileData("/sys/devices/system/cpu/cpu" + i + "/cpufreq/cpuinfo_max_freq") + FIELD_SEPARATOR;
                    message += "Minimum CPU" + i + " frequency: " + getFileData("/sys/devices/system/cpu/cpu" + i + "/cpufreq/cpuinfo_min_freq") + FIELD_SEPARATOR;
                }
                else {
                    message += getFileData("/sys/devices/system/cpu/cpu" + i + "/cpufreq/cpuinfo_max_freq") + FIELD_SEPARATOR;
                    message += getFileData("/sys/devices/system/cpu/cpu" + i + "/cpufreq/cpuinfo_min_freq") + FIELD_SEPARATOR;
                }
            }
        }
        message += CSV_SEPARATOR;
        //Log.w("ATTENTION","before sending");
        boolean communication_successful;
        InputStream cacrt,clientkey,clientcrt;
        String cacrt_file = "assets/ca.crt",clientkey_file="assets/client.key",clientcrt_file="assets/client.crt";
        cacrt = this.getClass().getClassLoader().getResourceAsStream(cacrt_file);
        clientcrt =this.getClass().getClassLoader().getResourceAsStream(clientcrt_file);
        clientkey = this.getClass().getClassLoader().getResourceAsStream(clientkey_file);
        String Device_data;
        if (verbose) {
            String CPU_data = "Number of CPU cores: " + getNumberOfCores()+CSV_SEPARATOR+message;
            String OS_data = "Device Information: OS Version: " + System.getProperty("os.version") + "(" + android.os.Build.VERSION.INCREMENTAL + ")" + CSV_SEPARATOR + " OS API Level: " + android.os.Build.VERSION.RELEASE + "(" + android.os.Build.VERSION.SDK_INT + ")" + CSV_SEPARATOR + " Device: " + android.os.Build.DEVICE + CSV_SEPARATOR + " Model (and Product): " + android.os.Build.MODEL + " (" + android.os.Build.PRODUCT + ")";
            //insert "a" after the protocol version, in order to specify that it will be used to announce data.
            Device_data = SensorDataPresenterActivity.COMMUNICATION_PROTOCOL_VERSION+"a,"+"Information for device: " + device_id + CSV_SEPARATOR + CPU_data + OS_data+" "+sdf.format(new Timestamp(System.currentTimeMillis()));
        }else {
            String CPU_data = getNumberOfCores()+CSV_SEPARATOR+message;
            String OS_data = System.getProperty("os.version") + "(" + android.os.Build.VERSION.INCREMENTAL + ")" + CSV_SEPARATOR + android.os.Build.VERSION.RELEASE + "(" + android.os.Build.VERSION.SDK_INT + ")" + CSV_SEPARATOR + android.os.Build.DEVICE + CSV_SEPARATOR + android.os.Build.MODEL + " (" + android.os.Build.PRODUCT + ")";
            //insert "a" after the protocol version, in order to specify that it will be used to announce data.
            Device_data = SensorDataPresenterActivity.COMMUNICATION_PROTOCOL_VERSION+"a,"+ device_id + CSV_SEPARATOR + CPU_data + OS_data+" "+CSV_SEPARATOR+sdf.format(new Timestamp(System.currentTimeMillis()));
        }
        Log.d("COMM","transmitting to SPEC topic");
        communication_successful = CommunicationClient.transmit(cacrt, clientkey,clientcrt, SPEC_MQTT_TOPIC,Device_data); //only case which transmits to the spec topic and not the real one.
        Log.d("COMM", "transmitted with result "+communication_successful);
        //TODO refine below code.

        if (!communication_successful){
            Timer timer = new Timer();
            timer.schedule(new TimerTask() {

                synchronized public void run() {
                    boolean successfuly_sent_file = sendDeviceFile();
                        if (successfuly_sent_file){
                        cancel();
                    }
                }

            }, 5*60*1000); //try to communicate every 5 minutes
        }

        //TODO refine above code

        //Log.w("ATTENTION","after sending");
        return true;
    }

    private static void initializeDeviceFile() {
        BufferedWriter bw;
        FileWriter fw;
        try {
            fw = new FileWriter(id_file, true);
            bw = new BufferedWriter(fw);
            device_id = android.os.Build.class.getField("SERIAL").get(null).toString();
        } catch (Exception e) {
            // String needs to be initialized
            // some value
            e.printStackTrace();
            return;
        }
        try {
            device_id += "-"+getRandomString(10);
            bw.write(device_id+"\n");
            bw.close();
        }catch (IOException e){
            e.printStackTrace();
            return;
        }
    }

    private static String getRandomString(final int sizeOfRandomString)
    {
        final Random random=new Random();
        final StringBuilder sb=new StringBuilder(sizeOfRandomString);
        for(int i=0;i<sizeOfRandomString;++i)
            sb.append(ALLOWED_CHARACTERS.charAt(random.nextInt(ALLOWED_CHARACTERS.length())));
        return sb.toString();
    }

    public static int getNumberOfCores() {
        if(Build.VERSION.SDK_INT >= 17) {
            return Runtime.getRuntime().availableProcessors();
        }
        else {
            return getNumCoresOldPhones();
        }
    }

    /**
     * Gets the number of cores available in this device, across all processors.
     * Requires: Ability to peruse the filesystem at "/sys/devices/system/cpu"
     * @return The number of cores, or 1 if failed to get result
     */
    private static int getNumCoresOldPhones() {
        //Private Class to display only CPU devices in the directory listing
        class CpuFilter implements FileFilter {
            @Override
            public boolean accept(File pathname) {
                //Check if filename is "cpu", followed by a single digit number
                if(Pattern.matches("cpu[0-9]+", pathname.getName())) {
                    return true;
                }
                return false;
            }
        }

        try {
            //Get directory containing CPU info
            File dir = new File("/sys/devices/system/cpu/");
            //Filter to only list the devices we care about
            File[] files = dir.listFiles(new CpuFilter());
            //Return the number of cores (virtual CPU devices)
            return files.length;
        } catch(Exception e) {
            //Default to return 1 core
            return 1;
        }
    }


    public String getCPUModel () {
        String cpu_model = null;
        try {

            BufferedReader br = new BufferedReader (new FileReader ("/proc/cpuinfo"));

            String str;

            while ((str = br.readLine ()) != null) {

                String[] data = str.split (":");

                if (data.length > 1) {

                    String key = data[0].trim().replace(" ", "_");
                    // if (key.equals ("model_name")) key = "cpu_model";
                    String value = data[1].trim();
                    if (key.equals("Hardware")) {
                        cpu_model = value.replaceAll("\\s+", " ");
                        cpu_model.replace(",", "_");
                    }
                }

            }

            br.close ();

        } catch (IOException e) {
            e.printStackTrace ();
        }

        return cpu_model;

    }


}
