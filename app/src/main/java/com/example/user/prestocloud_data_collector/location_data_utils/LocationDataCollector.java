package com.example.user.prestocloud_data_collector.location_data_utils;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.example.user.prestocloud_data_collector.Sensor_field;

import static com.example.user.prestocloud_data_collector.SensorDataPresenterActivity.sensors_status;
import static com.example.user.prestocloud_data_collector.SensorDataPresenterActivity.sensors_values;

public class LocationDataCollector extends Service implements LocationListener {
    public static final int GPS_REQUEST_INTERVAL = 10; //TODO refine;
    private static LocationManager locationManager;

    public LocationDataCollector() {
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {



    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //triggerUpdate();
        //initialization of the sensor values in order to have a constant number of fields
        synchronized (sensors_values) {
            sensors_values.set(Sensor_field.location, ",,");
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            //while (mContext==null); //TODO check this below and left!
            locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, GPS_REQUEST_INTERVAL, 0, this);
        }
        // We want this service to continue running until it is explicitly
        // stopped, so return sticky.
        return START_STICKY;
    }

    private LocationManager setupLocationManager() {
         locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        try {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, GPS_REQUEST_INTERVAL, this);
        }catch (SecurityException s){
            s.printStackTrace();
        }
        return locationManager;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onLocationChanged(Location location) {
        try {
            sensors_status.set(Sensor_field.location, "Latitude:" + location.getLatitude() + ",Longitude:" + location.getLongitude() + ",Altitude:" + location.getAltitude());
            //adapter.notifyDataSetChanged();
            //Log.d("Location change:","Detected");
            synchronized (sensors_values) {
                sensors_values.set(Sensor_field.location, location.getLatitude() + "," + location.getLongitude() + "," + location.getAltitude());
            }
        }catch(Exception e){
            synchronized (sensors_values) {
                sensors_values.set(Sensor_field.location, ",,");
            }
            e.printStackTrace();
            return;
        }
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.d("Latitude", "disable");
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.d("Latitude", "enable");
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.d("Latitude", "status");
    }
}
