package com.example.user.prestocloud_data_collector.battery_data_utils;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.IBinder;
import android.util.Log;

import static com.example.user.prestocloud_data_collector.SensorDataPresenterActivity.CSV_SEPARATOR;
import static com.example.user.prestocloud_data_collector.SensorDataPresenterActivity.sensors_status;
import static com.example.user.prestocloud_data_collector.SensorDataPresenterActivity.sensors_values;
import static com.example.user.prestocloud_data_collector.Sensor_field.battery;

public class BatteryDataCollector extends Service {

    BroadcastReceiver mBatInfoReceiver = null;

    public BatteryDataCollector() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //triggerUpdate();


        mBatInfoReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context ctxt, Intent intent) {


                    int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
                    int voltage = intent.getIntExtra(BatteryManager.EXTRA_VOLTAGE, -1);
                    int temperature = intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE,-1);
                    //String charge_status = intent.getStringExtra(BatteryManager.ACTION_CHARGING);
                    updateBatteryStatus(level, voltage, temperature);
                    //Log.d("BATTERY_STATUS",charge_status);


                }
            };
        registerReceiver(mBatInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

        return START_STICKY;
    }


    public void updateBatteryStatus(int pct, int voltage, int temperature) {
        boolean is_charging = isCharging();
        String verbose_charging_String = is_charging ? "Battery charging" : "Battery discharging";
        String simple_charging_String = is_charging ? "1" : "0";
        try {
            sensors_status.set(battery, "Battery percentage is " + pct + "% ,Battery voltage is "
                    + ((float) (voltage / 1000.0)) + "V, " + "Battery Temperature is " + temperature  + CSV_SEPARATOR+
                    verbose_charging_String);
            synchronized (sensors_values) {
                sensors_values.set(battery, pct + ";" + ((float) (voltage / 1000.0)) + ";" + temperature + ";" + simple_charging_String);
            }
        }
        catch (IndexOutOfBoundsException e){
            Log.e("Exception","Index out of bounds in battery data collector");
            return;
        }
    }

    public boolean isCharging() {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = this.registerReceiver(null, ifilter);
        int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        boolean bCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                status == BatteryManager.BATTERY_STATUS_FULL;
        return bCharging;
    }


    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
