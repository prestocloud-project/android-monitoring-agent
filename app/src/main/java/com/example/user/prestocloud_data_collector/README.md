## Instructions on the Deployment and usage of the Android Monitoring agent

**General Characteristics of the Application**

| Characteristic             | Details|
| -----------------------    |------- |
| Minimum SDK version        | 16 (Android 4.1 JellyBean) |
| Communication Protocol     | MQTT, over TLS |
| Data format                | CSV (commas to seperate first-level variables (numbered below), semicolons for second-level variables) |
| Broker used                                | Mosquitto |
| Device information collected (once)        |  [1] Information protocol version, [2] Unique Device Id, [3] CPU core number, [4] CPU name, [5] Maximum/Minimum frequency per CPU, [6] Linux Kernel version, [7] OS and API level, [8] Device Class, [9] Device model and product
|Monitored data (every 10 seconds)           | [1]  Information Protocol version, [2]  Unique Device Id, [3]  Current frequency for all CPU’s in Hz, [4]  Screen brightness level, [5]  Available RAM in Mb, [6]  Available RAM percentage, [7]  Available storage in Mb, [8]  Latitude, [9]  Longitude, [10] Altitude, [11] Wi-fi signal strength (quantified in 5 levels) and current status (on/off), [12] Active processes list or historical processes list, [13] Mobile network type, [14] Battery Percentage-Voltage-Temperature-Charging status (charging/discharging), [15] CPU consumption (percentage – average over last 3 minutes), [16] Timestamp  day.month.abbreviatedyear_hour.minute.second)

The application sends data which is gathered (roughly) every 10 seconds, every 30 minutes to the broker (or when the user presses the 'message' fab). As an intermediate step, the data is stored to a temporary file in the device storage.

The base activity of the application, which starts upon execution is `SensorDataPresenterActivity.java`, inside the `prestocloud_data_collector` package.

>**IMPORTANT:** In order for the application to be functional, the instructions below should be followed to update the source code and install the required certificate files.

### Source Code updates
The source code contains a number of variables that define the behaviour of the application with respect to data collection and communication.
The most important variables which should be set

| Class                  | Location                                                                                         | Details|
|------                  | ---------                                                                                        |--------|
|TransmitTriggerReceiver | Inside the `communication_utils` package, which is under the `prestocloud_data_collector` package| Should contain in its String brokerUri inside the onReceive method the address of the broker in the format: `tcp://broker.example.com`
|CommunicationClient | Located directly under the `prestocloud_data_collector ` package| 1) Should update the address of the broker by changing the BROKER class field to have a value with the following format: `ssl://broker.example.com:portNumber` 2) Should update the CLIENT_PASSWORD field to contain the password chosen for the certificate of the client |





### Generation of Certificates

The Android monitoring agent communicates with the Broker using TLS, and uses its own certificate files. A useful resource for the creation of the certificates is the manpage of mosquitto, also available online at https://mosquitto.org/man/mosquitto-tls-7.html

The following files should be placed inside a folder named `assets` directly under the `app/src/main` folder:

1) `ca.crt` - the file corresponding to the certificate authority issuing the TLS certificate

2) `client.crt` - the file corresponding to the certificate of the client

3) `client.key` - the file containing the private key of the client.


The Broker on the other hand should also employ the same certificates, and wait for messages with the following command (example using the Mosquitto Broker utilities):


```bash
mosquitto_sub -h localhost -t PrEstoCloud_Data --cafile ca.crt --cert client.crt --key client.key -q 2
```
