package com.example.user.prestocloud_data_collector.cpu_data_utils;

import android.util.Log;

import com.example.user.prestocloud_data_collector.SensorDataPresenterActivity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by user on 16/10/2017.
 */

public class TopThread extends Thread {

    public TopThread(String name){
        super(name);
    }

    @Override
    public void run() {
        int new_cpu_consumption = 0;
        String tempString= executeTop();

        //Log.d("CPU USAGE FROM TOP",tempString);
        String [] CpuUsages = tempString.replaceAll("[^\\d.]", " ").replaceAll("[ ]+"," ").trim().split(" ");
        for (int i = 0; i < CpuUsages.length; i++){
           if(acceptable_cpu_value(CpuUsages[i])){
                new_cpu_consumption += Integer.valueOf(CpuUsages[i]);
            }
            else{
                return;
            }
        }
        if (acceptable_cpu_value(new_cpu_consumption)){
            SensorDataPresenterActivity.cpu_consumption = new_cpu_consumption;
        }
    }

    private boolean acceptable_cpu_value(String cpuUsage) {
        if ((Integer.valueOf(cpuUsage)<0) || (Integer.valueOf(cpuUsage)>100)){
            return false;
        }
        else{
            return true;
        }
    }
    private boolean acceptable_cpu_value(int cpuUsage) {
        if ((cpuUsage<0) || (cpuUsage>100)){
            return false;
        }
        else{
            return true;
        }
    }

    private static String executeTop() {
        java.lang.Process p = null;
        BufferedReader in = null;
        String returnString = null;
        try {
            while (returnString == null || returnString.contentEquals("")) {

                p = Runtime.getRuntime().exec("top -n 1 -d 1");
                p.waitFor();
                if (p.exitValue()==136) continue; //erroneous process exit condition
                in = new BufferedReader(new InputStreamReader(p.getInputStream()));
                do {
                    returnString = in.readLine();
                } while (returnString == null || returnString.contentEquals(""));
                //Log.d("CPU_usage","TEST"+returnString);
                /*
                ProcessBuilder pb = new ProcessBuilder("top -n 1 -d 1");
                Map<String, String> enviornments = pb.environment();
                enviornments.put("TERM", "xterm-256color");
                java.lang.Process process = pb.start();
                */
                // p = Runtime.getRuntime().exec();
                //Thread.sleep(1000);
            }
        } catch (IOException e) {
            Log.e("executeTop", "error in getting first line of top");
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            try {
                if (in!=null) in.close();
                p.destroy();
            } catch (IOException e) {
                Log.e("executeTop",
                        "error in closing and destroying top process");
                e.printStackTrace();
            }
        }
        return returnString;
    }


}
