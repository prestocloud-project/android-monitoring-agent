package com.example.user.prestocloud_data_collector.storage_data_utils;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.IBinder;
import android.os.StatFs;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static com.example.user.prestocloud_data_collector.SensorDataPresenterActivity.sensors_status;
import static com.example.user.prestocloud_data_collector.SensorDataPresenterActivity.sensors_status_determined;
import static com.example.user.prestocloud_data_collector.SensorDataPresenterActivity.sensors_values;
import static com.example.user.prestocloud_data_collector.Sensor_field.storage_available;
import static java.lang.Math.min;

public class StorageDataCollector extends Service {
    public static final int SAMPLING_PERIOD_SECONDS = 10;

    public StorageDataCollector() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //triggerUpdate();



        ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
        scheduler.scheduleWithFixedDelay(new Runnable() {
            public void run() {

               AsyncTask getTopData = new AsyncTask() {
                    @Override
                    protected Object doInBackground(Object[] params) {
                        if (sensors_status_determined) //TODO determine maybe a better way to get the size of the data structure. This check here guarantees that the first initialization phase has been carried out (because the first calls to top might be stuck but the ensuing ones might be more successful.
                        {
                            try {
                                sensors_status.set(storage_available, "Storage available:  " + getFreeStorage() + "Mb");
                                synchronized (sensors_values) {
                                    sensors_values.set(storage_available, "" + getFreeStorage());
                                }
                            }catch (IndexOutOfBoundsException e){
                                return null;
                            }
                        }
                        return null;
                    }
                };

                getTopData.execute();
            }
        }, 0, SAMPLING_PERIOD_SECONDS, TimeUnit.SECONDS);

        // We want this service to continue running until it is explicitly
        // stopped, so return sticky.
        return START_STICKY;
    }
    private long getFreeStorage() {
        StatFs stat = new StatFs(Environment.getDataDirectory().getPath());
        long storage_bytes_available = (long) stat.getFreeBlocks() * (long) stat.getBlockSize();
        return  storage_bytes_available / 1048576;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
