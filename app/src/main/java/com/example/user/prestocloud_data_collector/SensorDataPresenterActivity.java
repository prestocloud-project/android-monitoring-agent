package com.example.user.prestocloud_data_collector;

import android.Manifest;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.AppOpsManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.user.prestocloud_data_collector.battery_data_utils.BatteryDataCollector;
import com.example.user.prestocloud_data_collector.brightness_data_utils.BrightnessDataCollector;
import com.example.user.prestocloud_data_collector.communication_utils.TransmitTriggerReceiver;
import com.example.user.prestocloud_data_collector.cpu_data_utils.CPUdataCollector;
import com.example.user.prestocloud_data_collector.cpu_data_utils.TopThread;
import com.example.user.prestocloud_data_collector.location_data_utils.LocationDataCollector;
import com.example.user.prestocloud_data_collector.network_data_utils.NetworkDataCollector;
import com.example.user.prestocloud_data_collector.process_data_utils.ProcessesDataCollector;
import com.example.user.prestocloud_data_collector.ram_data_utils.RAMdataCollector;
import com.example.user.prestocloud_data_collector.storage_data_utils.StorageDataCollector;
import com.example.user.prestocloud_data_collector.wifi_data_utils.WifiDataCollector;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class SensorDataPresenterActivity extends AppCompatActivity implements MqttCallback {

    public static final ArrayList<String> sensors_status = new ArrayList<String>();
    public static final ArrayList<String> sensors_status_presentation = new ArrayList<String>();
    public static final ArrayList<String> sensors_topics = new ArrayList<String>();
    public static final ArrayList<String> sensors_values = new ArrayList<String>(Sensor_field.number_of_fields);
    public static final ArrayList<String> sensors_values_transmission = new ArrayList<String>(Sensor_field.number_of_fields);
    public static final String CSV_SEPARATOR = ",";
    public static final String FIELD_SEPARATOR =";";
    public static final String DATA_MQTT_TOPIC ="PrEstoCloud_Data" ; //TODO rename to PrEstoCloud_Data
    public static final String SPEC_MQTT_TOPIC ="PrEstoCloud_Mobile_Spec" ;
    public static final int COMMUNICATION_PROTOCOL_VERSION =2;

    public static  ArrayAdapter<String> adapter;
    public static LocationManager locationManager;
    public static String location;
    public static int battery_percentage;
    public static float battery_voltage;
    public static int battery_temperature;
    public static String network_type;
    public static String wifi_sensor_status;
    public static int storage_available;
    public static double availableMegs,percentRamAvailable;
    public static String proc_state;
    public static int brightness_level;

    public static InputStream cacrt, clientkey, clientcrt;

    private static final int ADAPTER_SYNC_MILLIS = 100;
    private static final int MQTT_PUBLISH_INTERVAL = 30*60;     //Communication interval in seconds.
    // Once this number of seconds elapses all information currently on the information file gets sent through mqtt.
    ScheduledExecutorService scheduler;

    public static Context mContext=null;
    private ActivityManager activityManager;
    public static int cpu_consumption;
    public static boolean sensors_status_determined = false;

    private String initializationString = "...";
    private boolean mqtt_publisher_initialized = false;
    private boolean mqtt_subscriber_initialized = false;
    private boolean services_initialized = false;
    private boolean adapter_updates_requested = false;
    private String STATE_MQTT_PUB="The state of mqtt publisher";
    private String STATE_MQTT_SUB="The state of mqtt subscriber";
    private String SERVICES_INITIALIZED = "The state of the initialization of services";
    public SensorDataPresenterActivity() {
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState!=null){
            mqtt_publisher_initialized = savedInstanceState.getBoolean(STATE_MQTT_PUB);
            services_initialized = savedInstanceState.getBoolean(SERVICES_INITIALIZED);
        }
        synchronized (sensors_values) {
            initializeArraylist(sensors_values, Sensor_field.number_of_fields);
        }
        mContext = getApplicationContext();

        setContentView(R.layout.activity_sensors);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    cacrt = getAssets().open("ca.crt");
                    clientkey = getAssets().open("client.key");
                    clientcrt = getAssets().open("client.crt");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                boolean transmission_successful = CommunicationClient.transmit(cacrt, clientkey, clientcrt);
                if (transmission_successful) {
                    DeviceInformationManager.delete_data_collected_file = true;
                    Snackbar.make(view, "Data sent to the MQTT broker", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                }
            }
        });

        //TODO check for any difference setRecurringTransmitTrigger(this);

        //useful metrics are the following:
        //CPU
        //RAM usage
        //Storage space free (&usage)
        //Location data
        //Wifi signal strength
        //Processes running
        //Mobile network type
        //screen brightness
        //battery voltage

        if (!services_initialized) {
            requestPermissions(); // read from and write to sdcard.
            ensureUsageStatsPermissionGranted();
            services_initialized = true;
        }
        initializeSensorsStatus();

        initializeService(CommunicationClient.class);
        initializeService(CPUdataCollector.class);
        initializeService(RAMdataCollector.class);
        initializeService(WifiDataCollector.class);
        initializeService(BatteryDataCollector.class);
        initializeService(ProcessesDataCollector.class);
        initializeService(BrightnessDataCollector.class);
        initializeService(NetworkDataCollector.class);

    }

    private void initializeArraylist(ArrayList<String> arrayList, int capacity) {
        int initialSize = arrayList.size();
        for (int counter=0;counter<(capacity-initialSize);counter++){
            arrayList.add("");
        }
    }


    private void initializeSensorsStatus() {

        activityManager         =   (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        proc_state              =   "";//getCPUState();                // CPU state in GHz
        availableMegs           =   -1; // pending the real data coming from helper service            getAvailableMegs();           // Available RAM in Mb
        percentRamAvailable     =   -1; // getRamPercentAvailable();     // Available RAM percentage
        storage_available       =   -1; //getFreeStorage();             //Free Storage calculation
        wifi_sensor_status      =   "";//getWifiSensorStatus();        //Get wifi state
        brightness_level        =   -1;//getBrightnessLevel();         //Get brightness level
        network_type            =   "";//getTelephonyNetworkType();    //Get the type of the cellular network
        getCpuUsageStatistic();       //try to get the cpu consumption

        try {
            sensors_status.set(Sensor_field.device_id, "Device ID: " + value_or_Initializing(DeviceInformationManager.device_id,""));
        }catch (IndexOutOfBoundsException e) {
            sensors_status.add(Sensor_field.device_id, "Device ID: " + value_or_Initializing(DeviceInformationManager.device_id,""));
        }
        try{
            sensors_topics.set (Sensor_field.device_id,"Device ID");
        }catch (IndexOutOfBoundsException e){
            sensors_topics.add (Sensor_field.device_id,"Device ID");
        }

        try {
            sensors_status.set(Sensor_field.cpu_states, "CPU processing state (in Hz):  " + value_or_Initializing(proc_state, ""));
        }catch (IndexOutOfBoundsException e) {
            sensors_status.add(Sensor_field.cpu_states, "CPU processing state (in Hz):  " + value_or_Initializing(proc_state, ""));
        }
        try{
            sensors_topics.set(Sensor_field.cpu_states, "CPU");
        }catch (IndexOutOfBoundsException e){
            sensors_topics.add(Sensor_field.cpu_states, "CPU");
        }

        try {
            sensors_status.set(Sensor_field.brightness_level, "Screen brightness level:  " + value_or_Initializing(brightness_level, "/" + BrightnessDataCollector.MAX_BRIGHTNESS_VALUE));
        }catch (IndexOutOfBoundsException e) {
            sensors_status.add(Sensor_field.brightness_level, "Screen brightness level:  " + value_or_Initializing(brightness_level, "/" + BrightnessDataCollector.MAX_BRIGHTNESS_VALUE));
        }try{
            sensors_topics.set(Sensor_field.brightness_level, "Screen");
        }catch (IndexOutOfBoundsException e){
            sensors_topics.add(Sensor_field.brightness_level, "Screen");
        }

        try {
            sensors_status.set(Sensor_field.ram_available, "RAM available:  " + value_or_Initializing(availableMegs, "Mb ") + "Percentage free: " + value_or_Initializing(percentRamAvailable, "%"));
        }
        catch (IndexOutOfBoundsException e) {
            sensors_status.add(Sensor_field.ram_available, "RAM available:  " + value_or_Initializing(availableMegs, "Mb ") + "Percentage free: " + value_or_Initializing(percentRamAvailable, "%"));
        }
        try{
            sensors_topics.set(Sensor_field.ram_available, "RAM");
        }
        catch (IndexOutOfBoundsException e){
            sensors_topics.add(Sensor_field.ram_available, "RAM");
        }

        try {
            sensors_status.set(Sensor_field.storage_available, "Storage available:  " + value_or_Initializing(storage_available, "Mb"));
        }catch (IndexOutOfBoundsException e){
            sensors_status.add(Sensor_field.storage_available, "Storage available:  " + value_or_Initializing(storage_available, "Mb"));
        }
        try{
            sensors_topics.set(Sensor_field.storage_available, "Storage");
        }catch (IndexOutOfBoundsException e){
            sensors_topics.add(Sensor_field.storage_available, "Storage");
        }

        try {
            sensors_status.set(Sensor_field.location, "Current location:  " + value_or_Initializing(location, ""));
        }catch (IndexOutOfBoundsException e) {
            sensors_status.add(Sensor_field.location, "Current location:  " + value_or_Initializing(location, ""));
        }
        try{
            sensors_topics.set(Sensor_field.location, "Location");
        }catch (IndexOutOfBoundsException e){
            sensors_topics.add(Sensor_field.location, "Location");
        }
        try {
            sensors_status.set(Sensor_field.wifi, "Wifi signal strength:  " + value_or_Initializing(wifi_sensor_status, ""));
        }catch (IndexOutOfBoundsException e) {
            sensors_status.add(Sensor_field.wifi, "Wifi signal strength:  " + value_or_Initializing(wifi_sensor_status, ""));
        }try{
            sensors_topics.set(Sensor_field.wifi, "Wifi");
        }catch (IndexOutOfBoundsException e){
            sensors_topics.add(Sensor_field.wifi, "Wifi");
        }
        try {
            sensors_status.set(Sensor_field.proc_state, "Processes list is being retrieved..."); //only processes run during the last calendar day will be shown.
        }catch (IndexOutOfBoundsException e) {
            sensors_status.add(Sensor_field.proc_state, "Processes list is being retrieved..."); //only processes run during the last calendar day will be shown.
        }try{
            sensors_topics.set(Sensor_field.proc_state, "RunningProcs");
        }catch (IndexOutOfBoundsException e){
            sensors_topics.add(Sensor_field.proc_state, "RunningProcs");
        }
        try{
            sensors_status.set(Sensor_field.network_type, "Mobile network type:  " + value_or_Initializing(network_type, ""));
        }catch (IndexOutOfBoundsException e) {
            sensors_status.add(Sensor_field.network_type, "Mobile network type:  " + value_or_Initializing(network_type, ""));
        }try{
            sensors_topics.set(Sensor_field.network_type, "Network");
        }catch (IndexOutOfBoundsException e){
            sensors_topics.add(Sensor_field.network_type, "Network");
        }
        try {
            sensors_status.set(Sensor_field.battery, "Battery percentage:  " + value_or_Initializing(battery_percentage, "% ") + ",Battery voltage is " + value_or_Initializing(battery_voltage, "V "));
        }catch (IndexOutOfBoundsException e) {
            sensors_status.add(Sensor_field.battery, "Battery percentage:  " + value_or_Initializing(battery_percentage, "% ") + ",Battery voltage is " + value_or_Initializing(battery_voltage, "V "));
        }try{
            sensors_topics.set(Sensor_field.battery, "Battery");
        }catch (IndexOutOfBoundsException e) {
            sensors_topics.add(Sensor_field.battery, "Battery");
        }

        try {
            sensors_status.set(Sensor_field.cpu_consumption, "CPU Consumption: " + value_or_Initializing(cpu_consumption, ""));
        }catch (IndexOutOfBoundsException e) {
            sensors_status.add(Sensor_field.cpu_consumption, "CPU Consumption: " + value_or_Initializing(cpu_consumption, ""));
        }
        try{
            sensors_topics.set(Sensor_field.cpu_consumption, "CPU");
        }catch (IndexOutOfBoundsException e){
            sensors_topics.add(Sensor_field.cpu_consumption, "CPU");
        }


        sensors_status_determined = true;
//        Log.e ("Exception","Presentation arraylist changed 1");
        sensors_status_presentation.clear();
//        Log.e ("Exception","Presentation arraylist changed 2");
        sensors_status_presentation.addAll(sensors_status);
        Log.e ("Exception","2 OK");


        if (adapter == null) {
            adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, sensors_status_presentation);
        }
        else {
            adapter.notifyDataSetChanged();
        }
        ListView listView = (ListView) findViewById(R.id.mylistview);
        listView.setAdapter(adapter);
        requestAdapterUpdates();
    }

    private String value_or_Initializing(String s,String additional){
        if (s!=null && !s.isEmpty()){
            return s+additional;
        }else {
            return initializationString;
        }
    }
    private String value_or_Initializing(double d,String additional){
        if (d > 0){
            return String.valueOf(d)+additional;
        }else {
            return initializationString;
        }
    }
    private String value_or_Initializing(int i,String additional){
        if (i > 0){
            return String.valueOf(i)+additional;
        }else {
            return initializationString;
        }
    }


    private void initializeService(Class ServiceClass){
        startService(new Intent(this,ServiceClass));
    }

    private void stopRunningService(Class ServiceClass){
        stopService(new Intent(this, ServiceClass));
    }


    private void initializeMQTTDataPublisher() {
        mqtt_publisher_initialized = true;
        if (scheduler == null) {
            scheduler = Executors.newSingleThreadScheduledExecutor();
        }
        scheduler.scheduleWithFixedDelay
                (new Runnable() {
                    public void run() {
                        try {
                            cacrt = getAssets().open("ca.crt");
                            clientkey = getAssets().open("client.key");
                            clientcrt = getAssets().open("client.crt");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        boolean transmission_successful = CommunicationClient.transmit(cacrt, clientkey, clientcrt);
                        if (transmission_successful) {
                            DeviceInformationManager.delete_data_collected_file = true;
                        }
                    }
                }, 0, MQTT_PUBLISH_INTERVAL, TimeUnit.SECONDS);
    }

    private void initializeMQTTDataSubscriber(){
        CommunicationClient mqttSubscriber = new CommunicationClient();
        try {
            mqttSubscriber.subscribe(getAssets().open("ca.crt"),getAssets().open("client.crt"),getAssets().open("client.key"));
            mqtt_subscriber_initialized = true;
        } catch (IOException e) {
            e.printStackTrace();
        };
    }

    private void requestAdapterUpdates() {
        if (scheduler==null) {
            scheduler = Executors.newSingleThreadScheduledExecutor();
        }
        scheduler.scheduleWithFixedDelay
                (new Runnable() {
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
//                                Log.d ("CHANGE","Screen redraw happens now");
//                                Log.e ("Exception","Presentation arraylist changed 3");
                                sensors_status_presentation.clear();
//                                Log.e ("Exception","3 OK changing 4");
                                sensors_status_presentation.addAll(sensors_status);
//                                Log.e ("Exception","4 OK Testing adapter");
                                adapter.notifyDataSetChanged();
//                                Log.e ("Exception","Adapter OK");
                            }
                        });
                        //getCpuUsageStatistic();
                    }
                }, 0, ADAPTER_SYNC_MILLIS, TimeUnit.MILLISECONDS);
        adapter_updates_requested=true;
    }


    @Override
    protected void onStop() {
        //unregisterReceiver(mBatInfoReceiver);

        //DISABLE COMMENTS TO PREVENT BATTERY DRAIN THROUGH CONTINUOUS INFORMATION SENDING
        /*
        scheduler.shutdownNow();
        scheduler = null;
        */

        super.onStop();
    }

    @Override
    protected  void onPause() {
        //TODO ATTENTION! sensors_status_determined  = false;
//        Log.e ("Exception","Presentation arraylist changed 5");
        sensors_status_presentation.clear();
//        Log.e ("Exception","5 OK");

        adapter.clear();
        adapter.notifyDataSetChanged();
        super.onPause();
        //registerReceiver(mBatInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
    }


    @Override
    protected  void onResume() {
        super.onResume();
        if (!mqtt_publisher_initialized)initializeMQTTDataPublisher();
        if (!mqtt_subscriber_initialized)initializeMQTTDataSubscriber();
        if (!sensors_status_determined) initializeSensorsStatus();
        //registerReceiver(mBatInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem settingsItem = menu.findItem(R.id.action_settings);
        settingsItem.setVisible(false);
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sensors, menu);
        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(STATE_MQTT_PUB, mqtt_publisher_initialized);
        outState.putBoolean(SERVICES_INITIALIZED,services_initialized);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            makeToast("Functionality not implemented", Toast.LENGTH_SHORT);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }





    @Override
    public void connectionLost(Throwable cause) {

    }

    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        final String display_message = message.toString();
        runOnUiThread(new Runnable() {
            public void run() {
                makeToast("New Message! " + display_message, Toast.LENGTH_LONG);
            }
        });
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {

    }

    private void ensureUsageStatsPermissionGranted() {

        if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.KITKAT) {


            String permission = "android.permission.PACKAGE_USAGE_STATS";
            int res = getApplicationContext().checkCallingOrSelfPermission(permission);
            boolean permission_granted_usage_stats = false;

            AppOpsManager appOps = (AppOpsManager) getApplicationContext()
                    .getSystemService(Context.APP_OPS_SERVICE);
            int mode = appOps.checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS,android.os.Process.myUid(), getApplicationContext().getPackageName());


            if (mode == AppOpsManager.MODE_DEFAULT) {
                permission_granted_usage_stats = (res == PackageManager.PERMISSION_GRANTED);
            } else {
                permission_granted_usage_stats = (mode == AppOpsManager.MODE_ALLOWED);
            }


            if (!permission_granted_usage_stats) {
                Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
                startActivity(intent);
            }
        }
    }




/* migrated to Ramdatacollector

    private double getRamPercentAvailable() {

        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        activityManager.getMemoryInfo(mi);
        //Percentage can be calculated for API 16+
        return  (mi.availMem / (double) mi.totalMem);

    }


    private double getAvailableMegs() {

        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        activityManager.getMemoryInfo(mi);
        return mi.availMem / 0x100000L;

    }
*/



    private void requestPermissions(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!(checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) && (!(checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) && (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.ACCESS_FINE_LOCATION}, 200);
                Log.d("PERMISSION","No permission is granted");
            }
            else {
                if (!(checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) || !(checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 201);
                    Log.d("PERMISSION", "No permission to read or write from sd card");
                }
                else{
                    initializeService(StorageDataCollector.class);
                    initializeService(DeviceInformationManager.class);
                }
                if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 203);
                }
                else {
                    initializeService(LocationDataCollector.class);
                }
            }
        }
        else {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 204);
            }else {
                initializeService(LocationDataCollector.class);
            }
            initializeService(StorageDataCollector.class);
            initializeService(DeviceInformationManager.class);
        }
    }


    private void makeToast(String message,int duration){
        Toast.makeText(getBaseContext(), message,
                duration).show();
    }



    private void setRecurringTransmitTrigger(Context context) {

        Calendar updateTime = Calendar.getInstance();
        updateTime.setTimeZone(TimeZone.getTimeZone("GMT"));
        updateTime.set(Calendar.HOUR_OF_DAY, 12);
        updateTime.set(Calendar.MINUTE, 00);

        boolean TESTING = true;

        Intent sensorDataTransmission = new Intent(context, TransmitTriggerReceiver.class);

        if (!TESTING) {
            PendingIntent recurringDataTransmission = PendingIntent.getBroadcast(context,
                    0, sensorDataTransmission, PendingIntent.FLAG_CANCEL_CURRENT);
            AlarmManager alarms = (AlarmManager) this.getSystemService(
                    Context.ALARM_SERVICE);
            alarms.setInexactRepeating(AlarmManager.RTC_WAKEUP,
                    updateTime.getTimeInMillis(),
                    AlarmManager.INTERVAL_HALF_DAY, recurringDataTransmission);
        }else
        {
            startService(new Intent(context,CommunicationClient.class));
        }
    }


    private void getCpuUsageStatistic() {
        cpu_consumption = -100;
        //Threaded execution of executeTop() calls
        Thread topexecution = new TopThread("TOP execution thread");
        topexecution.start();
         /*

        result = 0;
        tempString=executeTop();

        String [] CpuUsages = tempString.replaceAll("[^\\d.]", " ").replaceAll("[ ]+"," ").trim().split(" ");
        for (int i = 0; i < CpuUsages.length; i++) {
            result += Integer.valueOf(CpuUsages[i]);
        }
        */

        /* OLD IMPLEMENTATION
        int cpuUsage=0;
        tempString = tempString.replaceAll(",", "");
        tempString = tempString.replaceAll("User", "");
        tempString = tempString.replaceAll("System", "");
        tempString = tempString.replaceAll("IOW", "");
        tempString = tempString.replaceAll("IRQ", "");
        tempString = tempString.replaceAll("%", "");
        for (int i = 0; i < 10; i++) {
            tempString = tempString.replaceAll("  ", " ");
        }
        tempString = tempString.trim();
        String[] myString = tempString.split(" ");
        int[] cpuUsageAsInt = new int[myString.length];
        for (int i = 0; i < myString.length; i++) {
            myString[i] = myString[i].trim();
            cpuUsage  += Integer.parseInt(myString[i]);
        }
        return cpuUsage;*/
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 200: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initializeService(DeviceInformationManager.class);
                    initializeService(StorageDataCollector.class);
                    initializeService(LocationDataCollector.class);
                } else {
                    return;
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            case 201: {
                initializeService(DeviceInformationManager.class);
                initializeService(StorageDataCollector.class);
            }
            case 203: {
                initializeService(LocationDataCollector.class);
            }
            case 204: {
                initializeService(LocationDataCollector.class);
            }

        }
    }



}
