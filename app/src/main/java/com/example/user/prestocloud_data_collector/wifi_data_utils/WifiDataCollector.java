package com.example.user.prestocloud_data_collector.wifi_data_utils;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static com.example.user.prestocloud_data_collector.SensorDataPresenterActivity.sensors_status;
import static com.example.user.prestocloud_data_collector.SensorDataPresenterActivity.sensors_status_determined;
import static com.example.user.prestocloud_data_collector.SensorDataPresenterActivity.sensors_values;
import static com.example.user.prestocloud_data_collector.Sensor_field.wifi;

public class WifiDataCollector extends Service {
    public static  final int SAMPLING_PERIOD_SECONDS = 10;
    public WifiDataCollector() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //triggerUpdate();



        ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
        scheduler.scheduleWithFixedDelay(new Runnable() {
            public void run() {

                AsyncTask getWifiData = new AsyncTask() {
                    @Override
                    protected Object doInBackground(Object[] params) {
                        if (sensors_status_determined) //TODO determine maybe a better way to get the size of the data structure. This check here guarantees that the first initialization phase has been carried out (because the first calls to top might be stuck but the ensuing ones might be more successful.
                        {
                            sensors_status.set(wifi, "Wifi signal strength:  " + getWifiSensorStatus(true));
                            synchronized (sensors_values) {
                                sensors_values.set(wifi, getWifiSensorStatus(false).toString());
                            }
                        }
                        return null;
                    }
                };

                getWifiData.execute();
            }
        }, 0, SAMPLING_PERIOD_SECONDS, TimeUnit.SECONDS);

        // We want this service to continue running until it is explicitly
        // stopped, so return sticky.
        return START_STICKY;
    }

    private String getWifiSensorStatus(boolean verbose) {
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        int numberOfLevels = 5;
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        int wifi_signal_strengthlevel = WifiManager.calculateSignalLevel(wifiInfo.getRssi(), numberOfLevels);
        String[] wifi_signal_strength_description = new String[]{"Bad", "Poor", "Average", "Good", "Excellent"};

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni != null && ni.getType() == ConnectivityManager.TYPE_WIFI) {
            Log.d("Success", "wifi is on and connected to " + wifiInfo.getBSSID());
            if (verbose) {
                return wifi_signal_strength_description[wifi_signal_strengthlevel] + " (" + (1 + wifi_signal_strengthlevel) + "/" + numberOfLevels + ")";
            } else {
                return String.valueOf(wifi_signal_strengthlevel)+getWifiEnabled(verbose);
            }
        } else {
            Log.d("Failure", "wifi is not connected");
            if (verbose) {
                return "Connection Unavailable - "+getWifiEnabled(verbose);
            } else {
                return "0"+getWifiEnabled(verbose);
            }
        }
    }

    private String getWifiEnabled(boolean verbose) {
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (verbose) {
            if (wifiManager.isWifiEnabled()) return "wifi is turned on.";
            else return "wifi is turned off";
        }else{
            if (wifiManager.isWifiEnabled()) return ";1";
            else return ";0";
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
