package com.example.user.prestocloud_data_collector.ram_data_utils;

import android.app.ActivityManager;
import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.user.prestocloud_data_collector.Sensor_field;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.example.user.prestocloud_data_collector.SensorDataPresenterActivity.sensors_status;
import static com.example.user.prestocloud_data_collector.SensorDataPresenterActivity.sensors_status_determined;
import static com.example.user.prestocloud_data_collector.SensorDataPresenterActivity.sensors_values;
import static java.lang.Math.min;

/**
 * Created by user on 18/10/2017.
 */

public class RAMdataCollector extends Service {

    public static final int SAMPLING_PERIOD_SECONDS = 10;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
        scheduler.scheduleWithFixedDelay(new Runnable() {
            public void run() {
                AsyncTask getRamData = new AsyncTask() {
                    @Override
                    protected Object doInBackground(Object[] params) {
                        if (sensors_status_determined) //TODO determine maybe a better way to get the size of the data structure. This check here guarantees that the first initialization phase has been carried out (because the first calls to top might be stuck but the ensuing ones might be more successful.
                        {
                            try {
                                sensors_status.set(Sensor_field.ram_available, "RAM available:  " + getAvailableMegs() + "Mb " + ",RAM Percentage free: " + getRamPercentAvailable() + "%");
                                synchronized (sensors_values) {
                                    sensors_values.set(Sensor_field.ram_available, "" + getAvailableMegs() + "," + getRamPercentAvailable());
                                }
                            }catch (IndexOutOfBoundsException e){
                                Log.e("Exception","Index out of bounds in Ram data collector");
                                return null;
                            }

                        }
                        return null;
                    }
                };
                getRamData.execute();
            }
        }, 0, SAMPLING_PERIOD_SECONDS, TimeUnit.SECONDS);

        return START_STICKY;
    }

    private double getAvailableMegs() {

        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        activityManager.getMemoryInfo(mi);
        return mi.availMem / 0x100000L;

    }

    private int getRamPercentAvailable() {

        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        activityManager.getMemoryInfo(mi);
        //TODO Percentage can be calculated for API 16+
        if (Build.VERSION.SDK_INT>16) {
            return (int) (100 * (mi.availMem / (double) mi.totalMem));
        }else{
            RandomAccessFile reader = null;
            String load = null;
            int totRam,totRamMB=0;
            String lastValue = "";
            try {
                reader = new RandomAccessFile("/proc/meminfo", "r");
                load = reader.readLine();
                // Get the Number value from the string
                Pattern p = Pattern.compile("(\\d+)");
                Matcher m = p.matcher(load);
                String value = "";
                while (m.find()) {
                    value = m.group(1);
                    // System.out.println("Ram : " + value);
                }
                reader.close();

                totRam = Integer.parseInt(value);
                totRamMB = totRam / 1024;

            } catch (IOException ex) {
                ex.printStackTrace();
            } finally {
                // Streams.close(reader);
            }
            return totRamMB;
        }
    }



    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
