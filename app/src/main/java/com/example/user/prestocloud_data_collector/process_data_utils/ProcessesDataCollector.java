package com.example.user.prestocloud_data_collector.process_data_utils;

import android.app.Service;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import com.jaredrummler.android.processes.AndroidProcesses;
import com.jaredrummler.android.processes.models.AndroidAppProcess;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static com.example.user.prestocloud_data_collector.SensorDataPresenterActivity.FIELD_SEPARATOR;
import static com.example.user.prestocloud_data_collector.SensorDataPresenterActivity.sensors_status;
import static com.example.user.prestocloud_data_collector.SensorDataPresenterActivity.sensors_status_determined;
import static com.example.user.prestocloud_data_collector.SensorDataPresenterActivity.sensors_values;
import static com.example.user.prestocloud_data_collector.Sensor_field.proc_state;

public class ProcessesDataCollector extends Service {

    public static final int SAMPLING_PERIOD_SECONDS = 10;

    public ProcessesDataCollector() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //triggerUpdate();
        ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
        scheduler.scheduleWithFixedDelay(new Runnable() {
            public void run() {

                        if (sensors_status_determined)
                        {
                            String running_processes_description,processes_list;
                            try {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    running_processes_description = "The processes that have run in the past 24 hours, are the following:";
                                    processes_list = getRunningProcessesNewAPI();
                                    running_processes_description = running_processes_description + processes_list;
                                    sensors_status.set(proc_state, running_processes_description); //only processes run during the last calendar day will be shown.
                                    synchronized (sensors_values) {
                                        sensors_values.set(proc_state, processes_list);
                                    }
                                } else {
                                    running_processes_description = "The processes running now, are the following: ";
                                    processes_list = getRunningProcessesOldAPI();
                                    running_processes_description = running_processes_description + processes_list;
                                    sensors_status.set(proc_state, running_processes_description);
                                    synchronized (sensors_values) {
                                        sensors_values.set(proc_state, processes_list);
                                    }
                                }
                            }catch (IndexOutOfBoundsException e){
                                Log.e("Exception","Index out of bounds inside processes data collector");
                            }
                        }

                };

        }, 0, SAMPLING_PERIOD_SECONDS, TimeUnit.SECONDS);

        // We want this service to continue running until it is explicitly
        // stopped, so return sticky.
        return START_STICKY;
    }



    //Below API covers cases up to Android 6
    public String getRunningProcessesOldAPI() {
        // Get a list of running apps
        List<AndroidAppProcess> processes = AndroidProcesses.getRunningAppProcesses();
        String processName="";
        String runningProcesses = "";

        for (AndroidAppProcess process : processes) {
            // Get some information about the process
            processName = process.name;
            /*
            Stat stat = null;
            try {
                stat = process.stat();
            } catch (IOException e) {
                e.printStackTrace();
            }

            int pid = stat.getPid();
            int parentProcessId = stat.ppid();
            long startTime = stat.stime();
            int policy = stat.policy();
            char state = stat.state();

            Statm statm = null;
            try {
                statm = process.statm();
            } catch (IOException e) {
                e.printStackTrace();
            }

            long totalSizeOfProcess = statm.getSize();
            long residentSetSize = statm.getResidentSetSize();
            */
            PackageInfo packageInfo = null;
            try {
                packageInfo = process.getPackageInfo(getBaseContext(), 0);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            //String appName = packageInfo.applicationInfo.loadLabel(getPackageManager()).toString();
            if (runningProcesses.equals("")) {
                runningProcesses = runningProcesses + secureHash(processName);
            }
            else {
                runningProcesses = runningProcesses + FIELD_SEPARATOR + secureHash(processName);
            }
        }
        return runningProcesses;
    }

    //TODO Dynamically update the processes running

    public String getRunningProcessesNewAPI() {

        SortedMap<Long, UsageStats> mySortedMap = new TreeMap<Long, UsageStats>();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            UsageStatsManager mUsageStatsManager = (UsageStatsManager) getSystemService(Context.USAGE_STATS_SERVICE);
            long time = System.currentTimeMillis();
            // We get usage stats for the last day
            List<UsageStats> stats = mUsageStatsManager.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, time - 24* 60 * 60, time);

            /*
            boolean usage_stats_not_determined = stats.isEmpty();
            if (usage_stats_not_determined) {
                //Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
                //startActivity(intent);
                time = System.currentTimeMillis();
                stats = mUsageStatsManager.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, time - 1000 * 10, time);
            }
            */


            // Sort the stats by the last time used
            //TODO check necessity of existence of if statement in below code block.
            if (stats != null) {
                for (UsageStats usageStats : stats) {
                    mySortedMap.put(usageStats.getLastTimeUsed(), usageStats);
                }
            }
        }

        String runningProcesses = "";
        int counter = 0;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            while (mySortedMap.size() > 0) {
                UsageStats FirstUsageStats = mySortedMap.get(mySortedMap.firstKey());
                if (counter>0) {
                    runningProcesses = runningProcesses + FIELD_SEPARATOR + secureHash(FirstUsageStats.getPackageName());
                }else{
                    runningProcesses = runningProcesses + secureHash(FirstUsageStats.getPackageName());
                }
                mySortedMap.remove(mySortedMap.firstKey());
                counter++;
            }
        }
        return runningProcesses;
    }



    private String secureHash(String packageName) {
        boolean hashing_activated = false; //TODO verify
        if (hashing_activated) {
            MessageDigest digest = null;
            try {
                digest = MessageDigest.getInstance("SHA-256");
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            digest.reset();
            return bin2hex(digest.digest(packageName.getBytes()));
        }
        else {
            return  packageName;
        }
    }

    private static String bin2hex(byte[] data) {
        return String.format("%0" + (data.length * 2) + 'x', new BigInteger(1, data));
    }


    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
