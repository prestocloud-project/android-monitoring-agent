package com.example.user.prestocloud_data_collector.brightness_data_utils;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.provider.Settings;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static com.example.user.prestocloud_data_collector.SensorDataPresenterActivity.sensors_status;
import static com.example.user.prestocloud_data_collector.SensorDataPresenterActivity.sensors_status_determined;
import static com.example.user.prestocloud_data_collector.SensorDataPresenterActivity.sensors_values;
import static com.example.user.prestocloud_data_collector.Sensor_field.brightness_level;

public class BrightnessDataCollector extends Service {
    public static final int SAMPLING_PERIOD_SECONDS = 30;
    public static final int MAX_BRIGHTNESS_VALUE = 255;
    public BrightnessDataCollector() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //triggerUpdate();



        ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
        scheduler.scheduleWithFixedDelay(new Runnable() {
            public void run() {

                AsyncTask getBrightnessData = new AsyncTask() {
                    @Override
                    protected Object doInBackground(Object[] params) {
                        if (sensors_status_determined) //TODO determine maybe a better way to get the state of the data structure. This check here guarantees that the first initialization phase has been carried out (because the first calls to top might be stuck but the ensuing ones might be more successful).
                        {
                            sensors_status.set(brightness_level, "Screen brightness level:  " + getBrightnessLevel()+"/" + MAX_BRIGHTNESS_VALUE);
                            synchronized (sensors_values) {
                                sensors_values.set(brightness_level, "" + getBrightnessLevel());
                            }
                        }
                        return null;
                    }
                };

                getBrightnessData.execute();
            }
        }, 0, SAMPLING_PERIOD_SECONDS, TimeUnit.SECONDS);

        // We want this service to continue running until it is explicitly
        // stopped, so return sticky.
        return START_STICKY;
    }

    private int getBrightnessLevel() {

        int curBrightnessValue;
        try {
            curBrightnessValue = Settings.System.getInt(
                    getContentResolver(), Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
            curBrightnessValue = -2;
        }
        return curBrightnessValue;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
