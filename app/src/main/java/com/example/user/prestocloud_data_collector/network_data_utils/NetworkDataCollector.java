package com.example.user.prestocloud_data_collector.network_data_utils;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.telephony.TelephonyManager;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static com.example.user.prestocloud_data_collector.SensorDataPresenterActivity.sensors_status;
import static com.example.user.prestocloud_data_collector.SensorDataPresenterActivity.sensors_status_determined;
import static com.example.user.prestocloud_data_collector.SensorDataPresenterActivity.sensors_values;
import static com.example.user.prestocloud_data_collector.Sensor_field.network_type;

public class NetworkDataCollector extends Service {
    public static final int SAMPLING_PERIOD_SECONDS = 60;
    public NetworkDataCollector() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //triggerUpdate();



        ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
        scheduler.scheduleWithFixedDelay(new Runnable() {
            public void run() {

                AsyncTask getNetworkData = new AsyncTask() {
                    @Override
                    protected Object doInBackground(Object[] params) {
                        if (sensors_status_determined) //TODO determine maybe a better way to get the size of the data structure. This check here guarantees that the first initialization phase has been carried out (because the first calls to top might be stuck but the ensuing ones might be more successful.
                        {
                            try {
                                sensors_status.set(network_type, "Mobile network type:  " + getTelephonyNetworkType());
                                synchronized (sensors_values) {
                                    sensors_values.set(network_type, getTelephonyNetworkType());
                                }
                            }catch (IndexOutOfBoundsException e){
                                return null;
                            }
                        }

                        return null;
                    }
                };

                getNetworkData.execute();
            }
        }, 0, SAMPLING_PERIOD_SECONDS, TimeUnit.SECONDS);

        // We want this service to continue running until it is explicitly
        // stopped, so return sticky.
        return START_STICKY;
    }


    private String getTelephonyNetworkType() {

        TelephonyManager teleMan =
                (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        int networkType = teleMan.getNetworkType();
        switch (networkType) {
            case TelephonyManager.NETWORK_TYPE_1xRTT:
                return "1xRTT";
            case TelephonyManager.NETWORK_TYPE_CDMA:
                return "CDMA";
            case TelephonyManager.NETWORK_TYPE_EDGE:
                return "EDGE";
            case TelephonyManager.NETWORK_TYPE_EHRPD:
                return "eHRPD";
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
                return "EVDO rev. 0";
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
                return "EVDO rev. A";
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
                return "EVDO rev. B";
            case TelephonyManager.NETWORK_TYPE_GPRS:
                return "GPRS";
            case TelephonyManager.NETWORK_TYPE_HSDPA:
                return "HSDPA";
            case TelephonyManager.NETWORK_TYPE_HSPA:
                return "HSPA";
            case TelephonyManager.NETWORK_TYPE_HSPAP:
                return "HSPA+";
            case TelephonyManager.NETWORK_TYPE_HSUPA:
                return "HSUPA";
            case TelephonyManager.NETWORK_TYPE_IDEN:
                return "iDen";
            case TelephonyManager.NETWORK_TYPE_LTE:
                return "LTE";
            case TelephonyManager.NETWORK_TYPE_UMTS:
                return "UMTS";
            case TelephonyManager.NETWORK_TYPE_UNKNOWN:
                return "Unknown";
        }

        return null;
    }



    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
