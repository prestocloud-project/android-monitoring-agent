package com.example.user.prestocloud_data_collector;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.widget.Toast;

import com.example.user.prestocloud_data_collector.communication_utils.SslUtil;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;

/*A few words on the function of this class.
* This class aims to do two things: Firstly, send the sensor data file correctly to an MQTT BROKER.
* Secondly, it aims to be able to send usage statistics NOW, to certain MQTT topics.
*
* For this reason, access to an MQTT client_crt is necessary, as well as to the data available.
* The methods below are implemented to send data twice a day.
*
*
* */


public class CommunicationClient extends Service implements MqttCallback {

    IBinder mBinder;
    public static final String BROKER = "ssl://broker.example.com:1883";
    public static final String CLIENT_PASSWORD = "";
    public static final String CLIENT_ID = "AndroidClient";
    public static final int QOS = 2;
    public static final MemoryPersistence PERSISTENCE = new MemoryPersistence();



    public static boolean transmit(InputStream cacrt, InputStream clientkey, InputStream clientcrt) {


        try {
            MqttClient sampleClient = new MqttClient(BROKER, CLIENT_ID, PERSISTENCE);
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            connOpts.setSocketFactory(SslUtil.getSocketFactory(cacrt, clientcrt, clientkey, CLIENT_PASSWORD));
            sampleClient.connect(connOpts);
            String message = "";


            if (DeviceInformationManager.persistence_file!=null) {
                message += getfileContentInSeparateLines(DeviceInformationManager.persistence_file)+"\n";
                sampleClient.publish(SensorDataPresenterActivity.DATA_MQTT_TOPIC, message.getBytes(), 2, false);
            }
            else{
                DeviceInformationManager.initializePersistenceFile();
            }
            sampleClient.disconnect();
            sampleClient.close();
            return true; //We have been successful in publishing the required information

        } catch (MqttException me) {
            me.printStackTrace();
            return  false; //handles connectivity issues by returning that the information was not properly transmitted.
        }
    }

    public static boolean transmit(InputStream cacrt, InputStream clientkey, InputStream clientcrt, String topic, String message) {


        try {
            MqttClient sampleClient = new MqttClient(BROKER, CLIENT_ID, PERSISTENCE);
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            connOpts.setSocketFactory(SslUtil.getSocketFactory(cacrt, clientcrt, clientkey, CLIENT_PASSWORD));
            sampleClient.connect(connOpts);

            sampleClient.publish(topic, message.getBytes(), 2, false);
            return true; //We have been successful in publishing the required information

        } catch (MqttException me) {
            me.printStackTrace();
            return false;  //handles connectivity issues by returning that the information was not properly transmitted.
        }
    }


    public void subscribe(InputStream cacrt,InputStream clientcrt,InputStream clientkey) {
        try {
            MqttClient subscriptionClient = new MqttClient(BROKER, "Subscribing Android Client", PERSISTENCE);
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            connOpts.setSocketFactory(SslUtil.getSocketFactory(cacrt, clientcrt, clientkey, CLIENT_PASSWORD));
            subscriptionClient.connect(connOpts);

            subscriptionClient.subscribe("AndroidMessaging",QOS);
            subscriptionClient.setCallback(this);



        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    public class LocalBinder extends Binder {
        CommunicationClient getService(){
            return CommunicationClient.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mBinder = new LocalBinder();

    }
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder; //Communication channel to the service, following https://developer.android.com/reference/android/app/Service.html
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Thread t = new Thread("MQTT connection and communication thread"){
            @Override
            public void run() {
                //setuptestMQTTConnection();
                //TODO perhaps call transmit here? Else this method can be deleted.
                stopSelf();
            }
        };
        t.start();
        return START_STICKY;
    }



    private static String getfileContentInSeparateLines(String filename) {
        BufferedReader br = null;
        FileReader fr = null;
        String fileContent = "";
        try {

            //br = new BufferedReader(new FileReader(FILENAME));
            File sdcard = Environment.getExternalStorageDirectory();
            File file = new File(sdcard,filename);
            fr = new FileReader(file);
            br = new BufferedReader(fr);
            StringBuffer sb = new StringBuffer();
            String sCurrentLine;

            while ((sCurrentLine = br.readLine()) != null) {
                sb.append(sCurrentLine);
            }
            fileContent =  sb.toString();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null)
                    br.close();
                if (fr != null)
                    fr.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return fileContent;
    }

    private static String getfileContentInSeparateLines(File file) {
        BufferedReader br = null;
        FileReader fr = null;
        String fileContent = "";
        try {
            fr = new FileReader(file);
            br = new BufferedReader(fr);
            StringBuffer sb = new StringBuffer();
            String sCurrentLine;

            while ((sCurrentLine = br.readLine()) != null) {
                sb.append(sCurrentLine);
                sb.append(System.getProperty("line.separator"));
            }
            fileContent =  sb.toString();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null)
                    br.close();
                if (fr != null)
                    fr.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return fileContent;
    }



    @Override
    public void connectionLost(Throwable cause) {

    }

    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        final String display_message = message.toString();
        Handler handler = new Handler(Looper.getMainLooper());

        handler.post(new Runnable() {
            public void run() {
                Toast.makeText(SensorDataPresenterActivity.mContext, "New Message! " + display_message, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {

    }
}
