package com.example.user.prestocloud_data_collector;

/**
 * Created by user on 18/10/2017.
 */

public class Sensor_field {

        public static final int device_id = 0;
        public static final int cpu_states = 1;
        public static final int brightness_level = 2;
        public static final int ram_available = 3;
        public static final int storage_available= 4;
        public static final int location= 5;
        public static final int wifi = 6;
        public static final int proc_state = 7;
        public static final int network_type= 8;
        public static final int battery= 9;
        public static final int cpu_consumption= 10;


        public static final int number_of_fields = 11; //TODO update this number if the number of fields changes
}
