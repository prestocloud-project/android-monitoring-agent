package com.example.user.prestocloud_data_collector.cpu_data_utils;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;

import com.example.user.prestocloud_data_collector.DeviceInformationManager;
import com.example.user.prestocloud_data_collector.SensorDataPresenterActivity;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static com.example.user.prestocloud_data_collector.SensorDataPresenterActivity.sensors_status;
import static com.example.user.prestocloud_data_collector.SensorDataPresenterActivity.sensors_status_determined;
import static com.example.user.prestocloud_data_collector.SensorDataPresenterActivity.sensors_values;
import static com.example.user.prestocloud_data_collector.Sensor_field.cpu_consumption;
import static com.example.user.prestocloud_data_collector.Sensor_field.cpu_states;
import static java.lang.Math.ceil;
import static java.lang.Math.min;

/**
 * Created by user on 21/9/2017.
         */

public class CPUdataCollector extends Service{
    private final int SAMPLING_PERIOD_SECONDS = 5;
    private final int MINUTES_CONSIDERED = 3; //get data only from the last 1 minute;
    private final int LOAD_ARRAY_SIZE = (int) ceil(MINUTES_CONSIDERED *60.0/SAMPLING_PERIOD_SECONDS);
    private int [] loadarray  = new int[LOAD_ARRAY_SIZE];
    private int loadcounter = 0;
    private int usagePercentage;
    private int load_avg;
    //int cpu_consumption;
    private String topOutput;
    LocalBroadcastManager broadcaster;

    public CPUdataCollector(){
    }
    @Override
    public void onCreate () {
        broadcaster = LocalBroadcastManager.getInstance(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //triggerUpdate();



        ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
        scheduler.scheduleWithFixedDelay(new Runnable() {
            public void run() {

                AsyncTask getTopData = new AsyncTask() {
                    @Override
                    protected Object doInBackground(Object[] params) {
                        //getCpuUsage();
                        TopThread topThread = new TopThread("repeating top command execution thread");
                        topThread.start();
                        if (sensors_status_determined) //TODO determine maybe a better way to get the size of the data structure. This check here guarantees that the first initialization phase has been carried out (because the first calls to top might be stuck but the ensuing ones might be more successful.
                        {
                            try {
                                sensors_status.set(cpu_consumption, "The CPU consumption is: " + load_avg);
                                sensors_status.set(cpu_states, getCPUStates(true));
                                synchronized (sensors_values) {
                                    sensors_values.set(cpu_consumption, "" + load_avg);
                                    sensors_values.set(cpu_states, getCPUStates(false));
                                }
                            }
                            catch (IndexOutOfBoundsException e){
                                return  null;
                            }
                        }
                        //Log.d("INFO","Usage percentage: "+usagePercentage);
                        //Log.d("INFO","Instance of CPUdataCollector service alive\n");
                        loadarray[loadcounter%loadarray.length]=SensorDataPresenterActivity.cpu_consumption; //array holds a moving window of CPU-usage snapshots
                        int load_sum = 0;
                        for (int cpu_load:loadarray){
                            load_sum += cpu_load;
                        }
                        loadcounter++;
                        load_avg = (int)(load_sum*1.0 / min(loadarray.length,loadcounter));
                        try {
                            topThread.join();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                };

                getTopData.execute();
            }
        }, 0, SAMPLING_PERIOD_SECONDS, TimeUnit.SECONDS);

        // We want this service to continue running until it is explicitly
        // stopped, so return sticky.
        return START_STICKY;
    }

    private String getCPUStates(boolean verbose) {
        BufferedReader br = null;
        FileReader fr = null;
        String CPUStates = "";
        for (int i=0;i< DeviceInformationManager.getNumberOfCores();i++) {
            String path = "/sys/devices/system/cpu/cpu" + i + "/cpufreq";
            String filename = "scaling_cur_freq";
            File f = new File(path, filename);

            if (f.exists()) {
                try {

                    //br = new BufferedReader(new FileReader(FILENAME));
                    fr = new FileReader(f);
                    br = new BufferedReader(fr);
                    StringBuffer sb = new StringBuffer();
                    String sCurrentLine;

                    while ((sCurrentLine = br.readLine()) != null) {
                        sb.append(sCurrentLine);
                    }
                    if (i==DeviceInformationManager.getNumberOfCores()-1) {
                       if (verbose) {
                           CPUStates += "CPU" + i + " frequency is " + sb.toString();
                       }
                       else {
                           CPUStates += sb.toString();
                       }
                    }else {
                       if (verbose) {
                           CPUStates += "CPU" + i + " frequency is " + sb.toString() + ";";
                       }
                       else {
                           CPUStates += sb.toString() + ";";
                       }
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    if (verbose) {
                        return "The CPU frequency cannot be determined for CPU" + i + ";";
                    }else {
                        return "";
                    }
                } finally {
                    try {
                        if (br != null)
                            br.close();
                        if (fr != null)
                            fr.close();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            } else {
                if (verbose) {
                    return "The CPU frequency cannot be determined for CPU" + i + ";";
                } else {
                    return "";
                }
            }
        }
        return  CPUStates;
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
